set xlabel "Duration (s)"
set ylabel "Size (GB)"
set grid
plot 'borgbench.dat' using 1:2:3 with labels point pt 7 offset 1,1 notitle
pause -1
